/*
  Simple Express Application for project deployment!
*/
let express = require('express');
let router = require('./routes/main');
let bodyParser = require('body-parser');
let mongoose = require('mongoose');
let app = express();

mongoose.connect('mongodb+srv://remote-aws-rnn:zpOaWSwAzaHhZgWH@rnndata-t3er7.mongodb.net/TicTacToeRNN?retryWrites=true&w=majority', { useNewUrlParser: true, useUnifiedTopology: true});
//FOR DEBUG PURPOSE: mongoose.connect("mongodb://localhost/TicTacToeRNN?retryWrites=true&w=majority", {useNewUrlParser: true, useUnifiedTopology: true});
if (mongoose.connection) {
  console.log("Successful connection");
  app.use(bodyParser.json());

  app.use(express.static('www'));
  router(app);

  let port = process.env.PORT || 3000;

  app.listen(port, ()=>{
    console.log("Listening on port", port);
  });
}
