let mongoose = require('mongoose')

let ConfigSchema = new mongoose.Schema({
  name: String,
  value: {}
});

module.exports = mongoose.model('Config', ConfigSchema);
