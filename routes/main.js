let router = require('express').Router();
let InstanceController = require('../controllers/InstanceController');

module.exports = (app) => {
    app.use('/ai', router);

    router.route('/registerInstance/:id')
      .post(InstanceController.register);

    router.route('/getInstance/:id')
      .get(InstanceController.getOne);

    router.route('/getNextInstance')
      .get(InstanceController.getNextInstance);
}
