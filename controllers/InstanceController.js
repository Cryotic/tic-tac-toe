let InstanceModel = require('../models/InstanceModel');
let Config = require('../models/ConfigModel');

class MatingPool{
  constructor(data){
    var totFitness = 0;
    var baseFitness = minFitness(data);
    for (var i = 0; i < data.length; i++){
      totFitness += data[i].score + baseFitness; // This should make every fitness to start from 0
    }
    this.mates = [];
    for (var i = 0; i < data.length; i++){
      this.mates.push({
        Wih: data[i].Wih,
        Who: data[i].Who,
        prob: (data[i].score + baseFitness)/totFitness
      });
    }
    this.wheel = [];
    for(var i = 0; i < this.mates.length; i++){
        for(var z = 0; z < Math.floor(this.mates[i].prob * 100); z++){
          this.wheel.push(i);
        }
    }

    this.length = this.mates.length;
  }

  getOne(){

    var selection = Math.floor(Math.random() * this.wheel.length);
    var id = this.wheel[selection];
    return this.mates[id];
  }
}

function createRandomMatrix(x, y){
  var base = [];
  for (var i = 0; i < x; i++){
    base[i] = []
    for(var j = 0; j < y; j++){
      base[i][j] = (Math.random() * -500) + 250;
    }
  }

  return base;
}

function minFitness(data){
  var min = data[0].score;
  for (var i = 1; i < data.length; i++){
    if (data[i].score < min) min = data[i].score;
  }

  return min;
}

function generateMatingPool(data){
    return new MatingPool(data);
}

function variate(arr){
  for (var i = 0; i < arr.length; i++){
    if (Math.random() < 0.1) arr[i] += (Math.random() % 500) - 250;
  }

  return arr;
}

function reproduce(a, b){
    var child = {Wih: [], Who: [], score: 0, gen: 0};
    for (var i = 0; i < a.Wih.length; i++){
      child.Wih.push(variate(Math.floor(Math.random() * 2) == 0 ? a.Wih[i] : b.Wih[i]));
    }
    for (var i = 0; i < a.Who.length; i++){
      child.Who.push(variate(Math.floor(Math.random() * 2) == 0 ? a.Who[i] : b.Who[i]));
    }
    return child;
}

module.exports.getOne = (req, res) => {
  InstanceModel.findOne({_id: req.params.id}, (err, result) => {
    if (err) res.status(500).json(err);
    if (result) res.json(result);
    else res.status(404).send("Resource not found");
  })
}

module.exports.register = (req, res) => {
  InstanceModel.findByIdAndUpdate(req.params.id, req.body, (err, result) => {
    if (err) res.send(500).json(err);
    if (res) res.json(result);
    else res.status(501).send("Impossible to save entity")
  })
}

module.exports.getNewGeneration = (callback) => {
  // This will be a little harder
  // There i will write a genetic algorithm
  // Basically I will randomly select a portion of weights of two entites and
  // then merge them together in a new entity and then return the new array
  // If there are not any entity saved in the database i will return a new array
  // of fresh RNNs.
  Config.findOne({name: "generation"}, (err, res) => {
    if (err || !res) {
      Config.create({name: "generation", value: {gen: 0}}, (err, resl)=>{});
      res = {};
      res.value = {};
      res.value.gen = null;
    }
    InstanceModel.find({gen: res.value.gen || 0, isExecuted: false}, (err, result)=>{
      if (err) callback(err, null);
      else{
        console.log(result.lenght);
        if (!result || result == [] || result.length == 0 ){
          if (res.value.gen == null){
            var newData = new Array();
            // Creates a base generation
            for (var i = 0; i < 50; i++){
              var data = {
                Wih: createRandomMatrix(19, 5),
                Who: createRandomMatrix(6, 9),
                isExecuted: false,
                score: 0,
                gen: 0
              }
              newData.push(data);
            }

            InstanceModel.create(newData, (err, res)=>{
              callback(err, res);
            });
            return;
          } else {
            Config.findOneAndUpdate({name: "generation"}, {value: {gen: (res.value.gen || 0) + 1}}, (err, old) => {
              if (err) callback(err, null);
              else {
                InstanceModel.find({gen: res.value.gen},(err, resl)=>{
                  console.log(res.value.gen);
                  console.log(resl)
                  if (err) {callback(err, null); return;}
                  var newData = [];
                  let matingPool = generateMatingPool(resl);
                  // I have to this 50 times
                  // I should save this info in config as "generation-lenght"
                  for(var i = 0; i < matingPool.length; i++){
                    newData.push(reproduce(matingPool.getOne(), matingPool.getOne()));
                    newData[i].gen = old.value.gen + 1;
                    newData[i].isExecuted = false;
                  }
                  InstanceModel.create(newData, (err, resl2)=>{
                    callback(err, resl2);
                  });
                });
              }
            });
          }
        } else {
          callback(null, result);
        }
      }
    });
  });
}

module.exports.getNextInstance = (req, res) => {
  module.exports.getNewGeneration((err, result)=>{
    console.log("Result length", result.length);
    console.log("No.\t|", "\tInstanceID\t|", "\tIsExecuted\t|", "\tGeneration")
    for (var i = 0; i < result.length; i++){
      console.log(i, result[i]._id, result[i].isExecuted, result[i].gen);
    }
    if (err) res.status(500).json(err);
    else res.json(result[0]);
  });
}
