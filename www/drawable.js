class Drawable{
  constructor(pos, size = createVector(0, 0), mesh = null, style = null){
    this.pos = pos;
    this.size = size;
    this.mesh = mesh;
    this.style = style;
    this.style.background = this.style.background || {color: null};
  }

  draw(){
    push();
    fill(this.style.background.color || '#000');
    strokeWeight(this.style.border.size || 1);
    stroke(this.style.border.color || this.style.border.color || '#0000');

    rect(this.pos.x, this.pos.y, this.size.x, this.size.y);
    if (this.mesh) this.mesh.draw(this.pos);
    pop();
  }
}
