let arr = [];
let n = 3;
var net;
let offset = 10;
let wd = 800;
let hd = 800;
var playerState = 0;
var gameState = [0, 0, 0, 0, 0, 0, 0, 0, 0];
let nets = new NetsAPI();


class ClickableSquare{
  constructor(pos, size, drawable, onClick){
    this.pos = pos;
    this.size = size;
    this.drawable = drawable;
    this.callback = onClick;
    this.wasClicked = false;
    this.isClicable = true;
    this.fc = -1;
    this.fs = [];

    this.fs.unshift(new Drawable(this.pos, createVector(this.size, this.size),
      new O(
        createVector(this.size/2, this.size/2), createVector(this.size, this.size)
      ), {
        border: {
          size: 1,
          color: '#FFFFFF'
        }
    }));

    this.fs.unshift(new Drawable(this.pos, createVector(this.size, this.size), new X(createVector(20, 20), createVector(this.size, this.size)), {
      border: {
        size: 1,
        color: '#FFFFFF'
      }
    }));

    var wd;
    wd = new Drawable(this.pos, createVector(this.size, this.size), null, {
      background: {
        color: '#000000'
      },
      border: {
        size: 1,
        color: '#FFFFFF'
      }
    })
    this.setDrawable(wd);
  }

  reset(){
    var wd;
    wd = new Drawable(this.pos, createVector(this.size, this.size), null, {
      background: {
        color: '#000000'
      },
      border: {
        size: 1,
        color: '#FFFFFF'
      }
    })
    this.setDrawable(wd);
    this.enableClick();
    this.wasClicked = false;

    document.getElementById("scoreboard").innerText = "Starting Next Match!";
  }

  clicked(){
    return this.wasClicked;
  }

  disableClick(){
    this.isClicable = false;
  }

  enableClick(){
    this.isClicable = true;
  }

  onClick(mouseX, mouseY){
    if (!this.isClicable) return;
    if (mouseX > this.pos.x && mouseX < this.pos.x + this.size && mouseY > this.pos.y && mouseY < this.pos.y + this.size){
      if (this.callback) this.callback(mouseX, mouseY);
      this.wasClicked = true;
    }
  }

  setDrawable(drawable){
    this.hasChanged();
    this.drawable = drawable;
  }

  draw(){
    if (this.clicked()){
      this.fc = playerState;

      this.setDrawable(this.fs[this.fc]);
      this.wasClicked = false;
    }
    if(this.drawable && this.hasChanges){
      this.drawable.draw();
      this.noNewChange();
    }
  }

  noNewChange(){
    this.hasChanges = false;
  }

  hasChanged(){
    this.hasChanges = true;
  }
}

function reset(){
  gameState = [0, 0, 0, 0, 0, 0, 0, 0, 0];
  for (var i = 0; i < arr.length; i++){
    arr[i].reset();
  }
}

function isGameEnded(){
  for (var i = 0; i < gameState.length; i++){
    if (gameState[i] == 0) return false;
  }

  return true;
}

function winner(){
  var ps = 1;
  for (; ps <= 2; ps++){
    // Case 1
    // * * *
    // . . .
    // . . .
    // left top -> right top
    var c = 0;
    for(var i = 0; i < 3; i++){
      if (gameState[i] == ps) c++;
    }
    if (c == 3) return ps;
    c = 0;

    // Case 2
    // * . .
    // . * .
    // . . *
    // left top -> right bottom
    for(var i = 0; i < 9; i += 4){
      if (gameState[i] == ps) c++;
    }
    if (c == 3) return ps;
    c = 0;

    // Case 3
    // * . .
    // * . .
    // * . .
    // left top -> left bottom
    for(var i = 0; i < 7; i += 3){
      if (gameState[i] == ps) c++;
    }
    if (c == 3) return ps;
    c = 0;

    // Case 4
    // . . *
    // . . *
    // . . *
    // right top -> right bottom
    for(var i = 2; i < 9; i += 3){
      if (gameState[i] == ps) c++;
    }
    if (c == 3) return ps;
    c = 0;

    // Case 5
    // . . *
    // . * .
    // * . .
    // right top -> left bottom
    for(var i = 2; i < 7; i += 2){
      if (gameState[i] == ps) c++;
    }
    if (c == 3) return ps;
    c = 0;

    // Case 6
    // . . .
    // . . .
    // * * *
    // left bottom -> right bottom
    for(var i = 6; i < 9; i++){
      if (gameState[i] == ps) c++;
    }
    if (c == 3) return ps;
    c = 0;


    // Case 7
    // . . .
    // * * *
    // . . .
    // Center left -> center right
    for(var i = 3; i < 6; i += 1){
      if (gameState[i] == ps) c++;
    }
    if (c == 3) return ps;
    c = 0;

    // Case 8
    // . * .
    // . * .
    // . * .
    // Center top -> center bottom
    for(var i = 1; i < 8; i += 3){
      if (gameState[i] == ps) c++;
    }
    if (c == 3) return ps;
  }
  return 0;
}

function firstEmpty(){
  for (var i = 0; i < gameState.length; i++) if (gameState[i] == 0) return i;
}
function setup() {
  createCanvas(wd, hd);
  w = (wd - 2*offset)/n;
  h = (hd - 2*offset)/n;
  for(var i = 0; i < (wd - 2*offset)/w; i++){
    for (var z = 0; z < (wd - 2*offset)/w; z++){
      arr.push(new ClickableSquare(createVector((z * w) + offset, (i * w) + offset), w, null));
      arr[i*z].disableClick();
    }
  }
  noLoop();
  nets.getNext((err, res) => {
    if(err){
      document.getElementById("scoreboard").innerText = "Error: " + err.status + ": " + err.error;
    }
    else {
      net = RecurrentNeuralNetwork.fromJSON(res);
      for (var i = 0; i < arr.length; i++){
        arr[i].enableClick();
      }
      loop();
    }
  });

  background(0);
}

function draw() {
  for (var i = 0; i < arr.length; i++){
    arr[i].draw();
  }

  if(isGameEnded() || winner() > 0){
      if (winner() == 0) document.getElementById("scoreboard").innerText = "Draw!";
      else document.getElementById("scoreboard").innerText = "Player " + (winner()) + " won!";
      if (winner() == 2){
        net.winner();
      } else if (winner() == 1){
        net.loser();
      } else {
        net.draw();
      }
      // There I have to do the transaction with the server, but i need to do it with infinite instances
      reset();
      noLoop();

      nets.register(net, (err, resl)=>{
        if (!err && resl) nets.getNext((err, result)=>{
          if (result){
            net = RecurrentNeuralNetwork.fromJSON(result);
            loop();
          }
          else if (err){
            document.getElementById("scoreboard").innerText = "Error: " + err.status + ": " + err.error;
          }
        });
        else if (err) document.getElementById("scoreboard").innerText = "Error: " + err.status + ": " + err.error;
      });
  }

  if (playerState == 1){
    var result;
    var tryCounter = 0;
    do {
      net.eval(gameState);
      result = net.getResult();
      tryCounter++;
    } while ((arr[result].wasClicked || !arr[result].isClicable) && tryCounter < 10000); // This may result in infinte loop
    if(tryCounter >= 10000) {net.punish(); result = firstEmpty();}// Goes to the first empty
    else {net.cookie();}
    arr[result].onClick(arr[result].pos.x + 10, arr[result].pos.y + 10);
    if(arr[result].wasClicked){
      gameState[result] = playerState + 1;
      arr[result].disableClick();
      playerState = 0;
      document.getElementById("scoreboard").innerText = "Player " + (playerState + 1) + "'s turn!";
    }
  }

  /*if (playerState == 0){
    var result;
    do {
      net.eval(gameState);
      result = net2.getResult();
      console.log(calculateMaxProbToWin());
    } while (arr[result].wasClicked); // This may result in infinte loop


    arr[result].onClick(arr[result].pos.x + 10, arr[result].pos.y + 10);
    if(arr[result].wasClicked){
      gameState[result] = playerState + 1;
      arr[result].disableClick();
      playerState = 1;
      document.getElementById("scoreboard").innerText = "Player " + (playerState + 1) + "'s turn!";
    }
  }*/
}

function mouseClicked(){
  if (playerState != 0) return;
  if(isGameEnded() || winner() > 0){
      if (winner() == 0) document.getElementById("scoreboard").innerText = "Draw!";
      else document.getElementById("scoreboard").innerText = "Player " + (winner()) + " won!";

      if (winner() == 2){
        net.winner();
      } else if (winner() == 1){
        net.loser();
      } else {
        net.draw();
      }

      reset();
      noLoop();
      nets.register(net, (err, resl)=>{
        if (!err && resl) nets.getNext((err, result)=>{
          if (result){
            net = RecurrentNeuralNetwork.fromJSON(result);
            loop();
          }
          else if (err){
            document.getElementById("scoreboard").innerText = "Error: " + err.status + ": " + err.error;
          }
        });
        else if (err) document.getElementById("scoreboard").innerText = "Error: " + err.status + ": " + err.error;
      });
  }
  for(var i = 0; i < arr.length; i++){
    arr[i].onClick(mouseX, mouseY);
    if(arr[i].wasClicked){
      gameState[i] = playerState + 1;
      arr[i].disableClick();
      playerState = 1;
      document.getElementById("scoreboard").innerText = "Player " + (playerState + 1) + "'s turn!";
    }
  }
}
