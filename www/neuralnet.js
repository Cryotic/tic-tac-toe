// So for the neural network i am going to use a genetic algorithm to train her.
// It will be fun to have two instances of a neural network and let them play each one against eachother.


/*
** size must be structured {x: value, y: value}
*/
function createRandomMatrix(x, y){
  var base = [];
  for (var i = 0; i < x; i++){
    base[i] = []
    for(var j = 0; j < y; j++){
      base[i][j] = (Math.random() * -500) + 250;
    }
  }
  return base;
}

function createArray(size, initialValue){
  var base = [];

  for(var i = 0; i < size; i++){
    base[i] = initialValue;
  }
  return base;
}

// copied from google, too lazy to figure out again how to build the algorithm
function matrixMult(a, b) {
  if(a[0].length != b.length) return [];
  var aNumRows = a.length, aNumCols = a[0].length,
      bNumRows = b.length, bNumCols = b[0].length,
      m = new Array(aNumRows);  // initialize array of rows
  for (var r = 0; r < aNumRows; ++r) {
    m[r] = new Array(bNumCols); // initialize the current row
    for (var c = 0; c < bNumCols; ++c) {
      m[r][c] = 0;             // initialize the current cell
      for (var i = 0; i < aNumCols; ++i) {
        m[r][c] += a[r][i] * b[i][c];
      }
    }
  }
  return m;
}

function sigmoid(x){
  return 1/(1 + Math.exp(-x));
}

function cp(arr){
  var cpy = []
  for(var i = 0; i < arr.length; i++){
    cpy[i] = arr[i];
  }
  return cpy;
}

function forEach(matr, callback){
  for(var i = 0; i < matr.length; i++){
    for(var j = 0; j < matr[0].length; j++){
      matr[i][j] = callback(matr[i][j])
    }
  }
  return matr;
}

function transpose(arr){
  arr[0].map((col, i) => arr.map(row => row[i]));
}

function conc(a, b){
  let newArr = new Array(a.length + b.length);
  for(var i = 0; i < a.length; i++){
    newArr[i] = a[i];
  }
  for(var i = 0; i < b.length; i++){
    newArr[a.length + i] = b[i];
  }
  return newArr;
}

function toMatrix(arr){
  var na = [];
  na[0] = cp(arr);
  return na;
}

class RecurrentNeuralNetwork{
   constructor(){
     this.points = 0;
     // Last state is also the output as I am using the genetic algorithm to teach the neural net.
     this.lastState = createArray(3*3, 0);
     this.input = new Array((3*3) + 1 /*Bias*/);
     this.input[this.input.length - 1] = 1; /*Bias is always 1*/
     // Weights from input to hidden layer
     this.Wih = createRandomMatrix(9/*this current input*/ + 9 /*last output*/ + 1 /*bias*/, 5);
     this.hidden = new Array((3*3) + 1 /*Bias*/);
     this.Woh = createRandomMatrix(5 + 1 /*Bias*/, 9);
     this.gen = 0;
     this.isExecuted = false;
     this.id = null;
   }

   static fromJSON(data){
     console.log(data);
     var rnn = new RecurrentNeuralNetwork();
     rnn.Wih = data.Wih;
     rnn.Woh = data.Who;
     rnn.gen = data.gen;
     rnn.id = data._id;
     return rnn;
   }

   eval(input){
     this.input = [];
     this.input = cp(input);
     this.input[9] = 1;
     this.hidden = forEach(
       matrixMult(
         toMatrix(
           conc(this.input, this.lastState)
         ),
         this.Wih
       ),
       sigmoid
     );

     this.hidden[0][5] = 1;

     this.lastState = forEach(
       matrixMult(
         this.hidden,
         this.Woh
       ),
       sigmoid
     );
     this.lastState = this.lastState[0];
   }

   getResult(){
     var maxIndex = 0;
     for (var i = 0; i < this.lastState.length; i++){
       if (this.lastState[i] > this.lastState[maxIndex]) maxIndex = i;
     }
     return maxIndex;
   }

   punish(){
     this.points -= 0.25;
   }

   cookie(){
     this.points += 0.5;
   }

   winner(){
     this.points = this.points + 1/2 * (this.points >= 0 ? this.points : this.points * -1);
     this.isExecuted = true;
   }

   loser(){
      this.points = this.points - 1/2 * (this.points >= 0 ? this.points : this.points * -1);
      this.isExecuted = true;
   }

   draw(){
     this.isExecuted = true;
   }
 }
