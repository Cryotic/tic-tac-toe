// Those are meshes

class X{
  constructor(relativePosition, windowSize){
    this.relPos = relativePosition;
    this.winSize = windowSize;
  }

  draw(basePos){
    push();
    stroke(255);
    strokeWeight(2);
    fill(0);

    line(basePos.x + this.relPos.x, basePos.y + this.relPos.y,
      basePos.x + this.winSize.x - this.relPos.x,
      basePos.y + this.winSize.y - this.relPos.y);

    line(basePos.x + this.relPos.x, basePos.y + this.winSize.y - this.relPos.y,
      basePos.x + this.winSize.x - this.relPos.x,
      basePos.y + this.relPos.y);

    pop();
  }
}

class O{
  constructor(relativePosition, windowSize){
    this.relPos = relativePosition;
    this.winSize = windowSize;
  }

  draw(basePos){
    push();
    stroke(255);
    strokeWeight(2);
    fill(0);
    circle(basePos.x + this.relPos.x, basePos.y + this.relPos.y, min(this.winSize.x, this.winSize.y) - 20);
    pop();
  }
}
