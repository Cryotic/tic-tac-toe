/*
  This API will perform transactions with the remote API
*/

class NetsAPI{
  constructor(){
    this.baseUrl = "http://localhost/ai";
    this.currentGeneration = 0; // This will be synchronized with the server
    this.nets = [];
  }

  getNext(callback){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        callback(null, JSON.parse(this.responseText));
      } else if (this.readyState == 4){
        callback({status: this.status, error: this.responseText}, null);
      }
    };
    xhttp.open("GET", this.baseUrl + "/getNextInstance", true);
    xhttp.send();
  }

  register(net, callback){
    // This will push info to the server
    var request = {isExecuted: true, score: 0};
    request.gen = net.gen;
    request.score = net.points;
    console.log(request);

    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        callback(null, JSON.parse(this.responseText));
      } else if (this.readyState == 4){
        callback({status: this.status, error: this.responseText}, null);
      }
    };


    xhttp.open("POST", this.baseUrl + "/registerInstance/" + net.id, true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(request));

    /*
      Perform a http post request to the server
    */
  }
}
